package com.c4.mavenMVC.UD22_Ejercicio3.model.service;

import javax.swing.JOptionPane;

import com.c4.mavenMVC.UD22_Ejercicio3.controller.ProyectoController;
import com.c4.mavenMVC.UD22_Ejercicio3.model.dao.ProyectoDao;
import com.c4.mavenMVC.UD22_Ejercicio3.model.dto.Proyecto;

public class ProyectoServ {

	private ProyectoController proyectoController; 
	public static boolean consultaProyecto=false;
	public static boolean modificaProyecto=false;

	//Metodo de vinculación con el controller principal
	public void setproyectoController(ProyectoController proyectoController) {
		this.setController(proyectoController);		
	}

	//Metodo que valida los datos de Registro antes de pasar estos al DAO
	public void validarRegistro(Proyecto miProyecto) {
		ProyectoDao miProyectoDao;
		if (miProyecto.getId().length()<=4) {
			miProyectoDao = new ProyectoDao();
			miProyectoDao.registrarProyecto(miProyecto);	
		}else{
			JOptionPane.showMessageDialog(null,"El id del proyecto debe ser menor o igual a 4 digitos","Advertencia",JOptionPane.WARNING_MESSAGE);
			modificaProyecto=false;
		}
		
	}

	//Metodo que valida los datos de consulta antes de pasar estos al DAO
	public Proyecto validarConsulta(String codigo) {
		ProyectoDao miProyectoDao;
		boolean consultaProyecto;
		try {
			miProyectoDao = new ProyectoDao();
			consultaProyecto = true;
			return miProyectoDao.buscarProyecto(codigo);
		} catch (Exception e) {
			consultaProyecto = false;
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
		}		
		return null;
	}
	
	//Metodo que valida los datos de Modificación antes de pasar estos al DAO
	public void validarModificacion(Proyecto miProyecto) {
		ProyectoDao miProyectoDao;
		if (miProyecto.getId().length()<=4) {
			miProyectoDao = new ProyectoDao();
			miProyectoDao.modificarProyecto(miProyecto);	
			modificaProyecto=true;
		}else{
			JOptionPane.showMessageDialog(null,"El id del proyecto debe ser menor o igual a 4 digitos","Advertencia",JOptionPane.WARNING_MESSAGE);
		}	
	}

	//Metodo que valida los datos de Eliminación antes de pasar estos al DAO
	public void validarEliminacion(String codigo) {
		ProyectoDao miClienteDao=new ProyectoDao();
		miClienteDao.eliminarProyecto(codigo);
	}

	
	
	public ProyectoController getProyectoController() {
		return proyectoController;
	}

	public void setController(ProyectoController proyectoController) {
		this.proyectoController = proyectoController;
	}



}
