package com.c4.mavenMVC.UD22_Ejercicio3.model.dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import com.c4.mavenMVC.UD22_Ejercicio3.model.conexion.ConnectionDB;
import com.c4.mavenMVC.UD22_Ejercicio3.model.dto.Asignado_A;
import com.c4.mavenMVC.UD22_Ejercicio3.model.dto.Cientifico;
import com.c4.mavenMVC.UD22_Ejercicio3.model.dto.Proyecto;



public class Asignado_ADao {

	public void registrarAsignado_A(Cientifico cientifico, Proyecto proyecto) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		try {
			boolean resultadoQuery;
			// Ejecutar Query que insrta el cliente en la base de datos
			resultadoQuery = conex.executeQuery("UD22-E3", "insert into Asignado_A(cientifico, proyecto) values ('" + cientifico.getDni() + "', '" + proyecto.getId() + "');");

			// Si devuelve false lanzamos un error!
			if (resultadoQuery == false) {
				throw new Exception();
			}
				
			// Cerrar conexion
			conex.closeConnection();
			
			JOptionPane.showMessageDialog(null, "Se ha registrado Exitosamente", "Información",JOptionPane.INFORMATION_MESSAGE);

		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se ha registrado", "Información", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public ArrayList<Asignado_A> listarAsignado_A() {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		boolean existe = false;
		try {
			ArrayList<Asignado_A> listaAsignado_A = new ArrayList<Asignado_A>();
			

			// Ejecutar Query que obtiene el cientifico, guardar los datos en el resultSet i
			// guardarlos despues en el objeto cientifico
			ResultSet res = conex.getValues("UD22-E3", "SELECT * FROM Asignado_A;");
			
			// Guardar los datos en el objeto cliente
			while (res.next()) {
				existe = true;
				Asignado_A asignado_A = new Asignado_A();
				asignado_A.setCientifico(res.getString("cientifico"));
				asignado_A.setProyecto(res.getString("proyecto"));
				listaAsignado_A.add(asignado_A);
			}
			// Cerrar conexion
			conex.closeConnection();
			
			// Si el cliente no existe, lanzar un error
			if (!existe)
				throw new Exception();
			
			return listaAsignado_A;
		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	public Asignado_A buscarAsignado_A(String cientifico, String proyecto) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		boolean existe = false;
		try {
			Asignado_A asignado_A = new Asignado_A();

			// Ejecutar Query que obtiene el cientifico, guardar los datos en el resultSet i
			// guardarlos despues en el objeto cientifico
			ResultSet res = conex.getValues("UD22-E3", "SELECT * FROM Asignado_A where cientifico = '" + cientifico + "' and proyecto = '" + proyecto + "';");

			// Si el cliente no existe, lanzar un error
			if (res == null)
				throw new Exception();
			
			// Guardar los datos en el objeto cliente
			while (res.next()) {
				existe = true;
				asignado_A.setCientifico(res.getString("cientifico"));
				asignado_A.setProyecto(res.getString("proyecto"));
			}
			// Cerrar conexion
			conex.closeConnection();

			// Si el cliente no existe, lanzar un error
			if (!existe)
				throw new Exception();
			
			return asignado_A;
		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	public void eliminarAsignado_A(String cientifico, String proyecto) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		try {
			boolean resultadoQuery;

			// Ejecutar Query que modifica el cliente en la base de datos
			resultadoQuery = conex.executeQuery("UD22-E3", "DELETE FROM Asignado_A where cientifico = '" + cientifico + "' and proyecto = '" + proyecto + "';");
			
			// Si devuelve false lanzamos un error!
			if (resultadoQuery == false) {
				throw new Exception();
			}
			
			// Cerrar conexion
			conex.closeConnection();
			
            JOptionPane.showMessageDialog(null, " Se ha Eliminado Correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
            
		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Elimino");
		}
	}

}
