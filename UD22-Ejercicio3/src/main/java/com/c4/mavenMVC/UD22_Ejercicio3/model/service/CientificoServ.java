package com.c4.mavenMVC.UD22_Ejercicio3.model.service;

import javax.swing.JOptionPane;

import com.c4.mavenMVC.UD22_Ejercicio3.controller.CientificoController;
import com.c4.mavenMVC.UD22_Ejercicio3.model.dao.CientificoDao;
import com.c4.mavenMVC.UD22_Ejercicio3.model.dto.Cientifico;


public class CientificoServ {

	private CientificoController cientificoController; 
	public static boolean consultaCientifico=false;
	public static boolean modificaCientifico=false;

	//Metodo de vinculación con el controller principal
	public void setcientificoController(CientificoController cientificoController) {
		this.setController(cientificoController);		
	}

	//Metodo que valida los datos de Registro antes de pasar estos al DAO
	public void validarRegistro(Cientifico miCientifico) {
		CientificoDao miCientificoDao;
		if (miCientifico.getNomApels().length()>5) {
		miCientificoDao = new CientificoDao();
		miCientificoDao.registrarCientifico(miCientifico);	
	}else{
		JOptionPane.showMessageDialog(null,"El Nombre Apels del cientifico debe ser mayor a 5 digitos","Advertencia",JOptionPane.WARNING_MESSAGE);
		modificaCientifico=false;
	}
	}

	//Metodo que valida los datos de consulta antes de pasar estos al DAO
	public Cientifico validarConsulta(String codigo) {
		CientificoDao miCientificoDao;
		boolean consultaCientifico;
		try {	
			miCientificoDao = new CientificoDao();
			consultaCientifico = true;
			return miCientificoDao.buscarCientifico(codigo);
		} catch (Exception e) {
			consultaCientifico = false;
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
		}		
		return null;
	}
	
	//Metodo que valida los datos de Modificación antes de pasar estos al DAO
	public void validarModificacion(Cientifico miCientifico) {
		CientificoDao miCientificoDao;
		if (miCientifico.getNomApels().length()>5) {
			miCientificoDao = new CientificoDao();
			miCientificoDao.modificarCientifico(miCientifico);	
			modificaCientifico=true;
		}else{
			JOptionPane.showMessageDialog(null,"El Nombre Apels del cientifico debe ser mayor a 5 digitos","Advertencia",JOptionPane.WARNING_MESSAGE);
		}	
	}

	//Metodo que valida los datos de Eliminación antes de pasar estos al DAO
	public void validarEliminacion(String codigo) {
		CientificoDao miCientificoDao=new CientificoDao();
		miCientificoDao.eliminarCientifico(codigo);
	}

	
	
	public CientificoController getCientificoController() {
		return cientificoController;
	}

	public void setController(CientificoController cientificoController) {
		this.cientificoController = cientificoController;
	}



}
