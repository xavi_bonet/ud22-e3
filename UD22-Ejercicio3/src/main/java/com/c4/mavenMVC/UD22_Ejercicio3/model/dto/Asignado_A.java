package com.c4.mavenMVC.UD22_Ejercicio3.model.dto;

public class Asignado_A {
	
	private String cientifico;
	private String proyecto;

	public Asignado_A(){
	}

	public Asignado_A(String cientifico, String proyecto) {
		this.cientifico = cientifico;
		this.proyecto = proyecto;
	}

	public String getCientifico() {
		return cientifico;
	}

	public void setCientifico(String cientifico) {
		this.cientifico = cientifico;
	}

	public String getProyecto() {
		return proyecto;
	}

	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}
	
	

}
