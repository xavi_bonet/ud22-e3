CREATE DATABASE IF NOT EXISTS `UD22-E3`;
USE `UD22-E3`;

DROP TABLE IF EXISTS `Cientificos`;
DROP TABLE IF EXISTS `Proyecto`;
DROP TABLE IF EXISTS `Asignado_A`;

CREATE TABLE Cientificos (
	dni VARCHAR(8), 
	nom_apels NVARCHAR(255),
   	PRIMARY KEY (dni)
);

CREATE TABLE Proyecto (
	id_proyecto CHAR(4), 
	nombre NVARCHAR(255),
	horas INT,
	PRIMARY KEY (id_proyecto)
);

CREATE TABLE Asignado_A (
	cientifico VARCHAR(8), 
	proyecto CHAR(4), 
	PRIMARY KEY (cientifico, proyecto), 
	FOREIGN KEY (cientifico) REFERENCES Cientificos(dni),
	FOREIGN KEY (proyecto) REFERENCES Proyecto(id_proyecto)
);

insert into Cientificos(dni, nom_apels) values 
('1234567A', 'test'),
('2234567A', 'test'),
('3234567A', 'test'),
('4234567A', 'test'),
('5234567A', 'test'),
('6234567A', 'test'),
('7234567A', 'test'),
('8234567A', 'test'),
('9234567A', 'test'),
('1034567A', 'test');

insert into Proyecto(id_proyecto, nombre, horas) values 
('A1', 'nombre1', 1),
('A2', 'nombre2', 2),
('A3', 'nombre3', 3),
('A4', 'nombre4', 4),
('A5', 'nombre5', 5),
('A6', 'nombre6', 6),
('A7', 'nombre7', 7),
('A8', 'nombre8', 8),
('A9', 'nombre9', 9),
('A10', 'nombre10', 10);

insert into Asignado_A(cientifico, proyecto) values 
('1234567A', 'A1'),
('2234567A', 'A2'),
('3234567A', 'A3'),
('4234567A', 'A4'),
('5234567A', 'A5'),
('6234567A', 'A6'),
('7234567A', 'A7'),
('8234567A', 'A8'),
('9234567A', 'A9'),
('1034567A', 'A10');

select * from Cientificos;
select * from Proyecto;
select * from Asignado_A;